package emiratsauction.com.ea_androidtask.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import emiratsauction.com.ea_androidtask.R;
import emiratsauction.com.ea_androidtask.model.Car;


public class CarsListAdapter extends RecyclerView.Adapter<CarsListAdapter.ViewHolder> implements Filterable {


    ArrayList<Car> Cars;
    Context Context;
    boolean[] animationStates;
    public static int CarPosition;
    public String timeleft_Ticks;
    List<Car> carList =null;
    List<Car> filteredCarList = null;
    CarFilter carFilter;
    public static int VisibleOnce = 0;


    @Override
    public Filter getFilter() {
        if(carFilter == null)
            carFilter = new CarFilter(this, carList);
        return carFilter;
    }

    //Constractor
    public CarsListAdapter(Context context, ArrayList<Car> cars) {
        Context = context;
        Cars = cars;
      //  timeleft_Ticks= Ticks;
        this.carList =cars;
        this.filteredCarList = new ArrayList<>();
        animationStates = new boolean[Cars.size()];
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView CarBanner;
        public ImageView CarLogo;
        public TextView CarName;
        public TextView Txt_CarCurrentPrice;
        public TextView Txt_CarCurrrency;
        public TextView Txt_Car_LotNo;
        public TextView Txt_Car_Bids;
        public TextView Txt_Car_UpdateTime;
      //  public LinearLayout CarShadowLinear;
//        public LinearLayout CarBannerContainer;
        public RelativeLayout CarDetailsRelative;
        public CardView CarCard;


        public ViewHolder(final View ConvertView) {
            super(ConvertView);

            CarLogo = (ImageView) ConvertView.findViewById(R.id.Img_CarLogo);
            CarName = (TextView) ConvertView.findViewById(R.id.Txt_CarName);
            Txt_CarCurrentPrice = (TextView) ConvertView.findViewById(R.id.Txt_CarCurrentPrice);
            Txt_CarCurrrency = (TextView) ConvertView.findViewById(R.id.Txt_CarCurrrency);
            Txt_Car_LotNo = (TextView) ConvertView.findViewById(R.id.Txt_Car_LotNo);
            Txt_Car_Bids = (TextView) ConvertView.findViewById(R.id.Txt_Car_Bids);
            Txt_Car_UpdateTime = (TextView) ConvertView.findViewById(R.id.Txt_Car_UpdateTime);
           // CarShadowLinear = (LinearLayout) ConvertView.findViewById(R.id.L_CarShadow);
//            CarBannerContainer = ConvertView.findViewById(R.id.L_CarBannerContainer);
            CarDetailsRelative = ConvertView.findViewById(R.id.R_CarDetails);
            CarCard = (CardView) ConvertView.findViewById(R.id.Card_CarCard);

//            if(VisibleOnce == 0) {
//                CarShadowLinear.setVisibility(View.GONE);
//            //    CarBannerContainer.setVisibility(View.GONE);
//          //      CarDetailsRelative.setVisibility(View.GONE);
//                CarLogo.setVisibility(View.INVISIBLE);
//            }
//
//            VisibleOnce++;



            ConvertView.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NewApi")
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

               //     Shake(CarLogo,Context);
                //    Move(CarCard , CarCard.getWidth(), 0 ,1000);


                /*    MoveobjectAnimator.addListener(new Animator.AnimatorListener() {
                        public void onAnimationStart(Animator animation) {}
                        @Override
                        public void onAnimationEnd(Animator animation) {

                          //  CarPosition = getAdapterPosition();
                       //     Intent intent = new Intent(Context, DetailsActivity.class);
                        //    ConvertView.getContext().startActivity(intent);
                        }
                        public void onAnimationCancel(Animator animation) {}
                        public void onAnimationRepeat(Animator animation) {}
                    });*/

                }
            }
            );


        }
    }

    //Create ViewHolder & Intitialize it
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_row, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    public void resetData( ArrayList<Car>  CarsList) {
        this.Cars = CarsList;
        this.notifyDataSetChanged();
    }
    //Fill The List
    @SuppressLint("ResourceAsColor")
    public void onBindViewHolder(final ViewHolder Holder, int position) {


        Car currentCar = Cars.get(position);

       // Holder.CarLogo.setImageResource(currentCar.getImage());
        Log.d("CarsListAdapter", "onBindViewHolder:image url: "+currentCar.getImage());
        String [] imageUrl=currentCar.getImage().split("t_,w");

        Picasso.with(Context)
                .load(imageUrl[0]+"t_,w_220,h_250/images.jpg")
                .placeholder(R.drawable.imagecar)
                .error(R.drawable.imagecar)
                .resize(300,300)
                .into(Holder.CarLogo);
        Holder.CarName.setText(currentCar.getMakeEn()+" "+currentCar.getModelEn()+" "+currentCar.getYear());
        Holder.Txt_CarCurrentPrice.setText(currentCar.getAuctionInfo().getCurrentPrice()+"");
        Holder.Txt_CarCurrrency.setText(currentCar.getAuctionInfo().getCurrencyEn());
        Holder.Txt_Car_LotNo.setText(currentCar.getAuctionInfo().getLot()+"");
        Holder.Txt_Car_Bids.setText(currentCar.getAuctionInfo().getBids()+"");
        BigDecimal ticksinseconds=new BigDecimal(currentCar.getAuctionInfo().getEndDate());
        long longVal = ticksinseconds.longValue();
         long hours = longVal / 3600;
       // long days = hours / 24;
        long  minutes = (longVal % 3600) / 60;
        long seconds = longVal % 60;
        if (hours == 0 && minutes < 5){
            Holder.Txt_Car_UpdateTime.setTextColor(Color.RED);
        }

        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        Holder.Txt_Car_UpdateTime.setText(timeString);


//        if (!animationStates[position]) {
//            animationStates[position] = true;
//            Animation animation = AnimationUtils.loadAnimation(Context, R.anim.transaction_right);
//            animation.setStartOffset(position*150);
//            Holder.CarCard.startAnimation(animation);
//
//            animation.setAnimationListener(new Animation.AnimationListener() {
//                public void onAnimationStart(Animation animation) {}
//                @Override
//                public void onAnimationEnd(Animation animation) {
//                    Holder.CarLogo.setVisibility(View.VISIBLE);
//                    Holder.CarShadowLinear.setVisibility(View.VISIBLE);
//                  //  Holder.CarBannerContainer.setVisibility(View.VISIBLE);
//                  //  Holder.CarDetailsRelative.setVisibility(View.VISIBLE);
//
//
//                }
//                public void onAnimationRepeat(Animation animation) {}
//            });
//       }

    }


    public int getItemCount() {
        return Cars.size();
    }
    private static class CarFilter extends Filter {

        private final CarsListAdapter adapter;

        private final List<Car> originalList;

        private final List<Car> filteredList;

        private CarFilter(CarsListAdapter adapter, List<Car> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new ArrayList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
                Log.i("resturentaddbyoriginal", "ifperformFiltering: ");
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
              //  Pattern pattern = Pattern.compile(filterPattern, Pattern.CASE_INSENSITIVE);
                  Log.i("resturantad", "elseperformFiltering: ");
                for (final Car car : originalList) {
                   // Pattern xpattern = Pattern.compile(car.getCarName(), Pattern.CASE_INSENSITIVE);
                    if (car.getMakeEn().toLowerCase().contains(filterPattern)) {
                        filteredList.add(car);
                    }
                     Log.i("car_added", "notperformFiltering: ");
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredCarList.clear();
            adapter.filteredCarList.addAll((ArrayList<Car>) results.values);
            adapter.Cars.clear();
            adapter.Cars.addAll(filteredList);
            adapter.notifyDataSetChanged();
        }
    }

}
