package emiratsauction.com.ea_androidtask.customViewsAndContollers;


import android.app.Application;
import android.content.Context;

/**
 * Created by Mahmoud on 07/07/2018.
 */
public class ApplicationController extends Application {


    private static ApplicationController instance;
    private static Context mContext;

    public synchronized static ApplicationController getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = this;

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
