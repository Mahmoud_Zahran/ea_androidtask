package emiratsauction.com.ea_androidtask.customViewsAndContollers;


import android.app.ProgressDialog;
import android.content.Context;

import emiratsauction.com.ea_androidtask.R;


/**
 * Created by Mahmoud on 07/07/2018.
 */

public class CustomProgressDialog {

    public static ProgressDialog create(Context context, String msg, int theme){

        ProgressDialog progressDialog = new ProgressDialog(context, theme);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        //progressBar.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return progressDialog;
    }

    public static ProgressDialog create(Context context){

        ProgressDialog progressDialog = new ProgressDialog(context, R.style.CustomDialog);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(context.getString(R.string.pleaseWait));
        progressDialog.setCancelable(false);
        //progressBar.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return progressDialog;
    }
}
