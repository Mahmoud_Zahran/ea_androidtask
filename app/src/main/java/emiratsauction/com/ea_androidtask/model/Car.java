package emiratsauction.com.ea_androidtask.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mahmoud on 07/07/2018.
 */

public class Car implements Serializable {

    @SerializedName("carID")
    int carID;
    @SerializedName("image")
    String image;
    @SerializedName("descriptionAr")
    String descriptionAr;
    @SerializedName("descriptionEn")
    String descriptionEn;
    @SerializedName("imgCount")
    int imgCount;
    @SerializedName("sharingLink")
    String sharingLink;
    @SerializedName("sharingMsgEn")
    String sharingMsgEn;
    @SerializedName("sharingMsgAr")
    String sharingMsgAr;
    @SerializedName("mileage")
    String mileage;
    @SerializedName("makeID")
    int makeID;
    @SerializedName("modelID")
    int modelID;
    @SerializedName("bodyId")
    int bodyId;
    @SerializedName("year")
    int year;
    @SerializedName("makeEn")
    String makeEn;
    @SerializedName("makeAr")
    String makeAr;
    @SerializedName("modelEn")
    String modelEn;
    @SerializedName("modelAr")
    String modelAr;
    @SerializedName("bodyEn")
    String bodyEn;
    @SerializedName("bodyAr")
    String bodyAr;
    @SerializedName("AuctionInfo")
    AuctionInfo AuctionInfo;

    public int getCarID() {
        return carID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public int getImgCount() {
        return imgCount;
    }

    public void setImgCount(int imgCount) {
        this.imgCount = imgCount;
    }

    public String getSharingLink() {
        return sharingLink;
    }

    public void setSharingLink(String sharingLink) {
        this.sharingLink = sharingLink;
    }

    public String getSharingMsgEn() {
        return sharingMsgEn;
    }

    public void setSharingMsgEn(String sharingMsgEn) {
        this.sharingMsgEn = sharingMsgEn;
    }

    public String getSharingMsgAr() {
        return sharingMsgAr;
    }

    public void setSharingMsgAr(String sharingMsgAr) {
        this.sharingMsgAr = sharingMsgAr;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public int getMakeID() {
        return makeID;
    }

    public void setMakeID(int makeID) {
        this.makeID = makeID;
    }

    public int getModelID() {
        return modelID;
    }

    public void setModelID(int modelID) {
        this.modelID = modelID;
    }

    public int getBodyId() {
        return bodyId;
    }

    public void setBodyId(int bodyId) {
        this.bodyId = bodyId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMakeEn() {
        return makeEn;
    }

    public void setMakeEn(String makeEn) {
        this.makeEn = makeEn;
    }

    public String getMakeAr() {
        return makeAr;
    }

    public void setMakeAr(String makeAr) {
        this.makeAr = makeAr;
    }

    public String getModelEn() {
        return modelEn;
    }

    public void setModelEn(String modelEn) {
        this.modelEn = modelEn;
    }

    public String getModelAr() {
        return modelAr;
    }

    public void setModelAr(String modelAr) {
        this.modelAr = modelAr;
    }

    public String getBodyEn() {
        return bodyEn;
    }

    public void setBodyEn(String bodyEn) {
        this.bodyEn = bodyEn;
    }

    public String getBodyAr() {
        return bodyAr;
    }

    public void setBodyAr(String bodyAr) {
        this.bodyAr = bodyAr;
    }

    public AuctionInfo getAuctionInfo() {
        return AuctionInfo;
    }

    public void setAuctionInfo(AuctionInfo auctionInfo) {
        AuctionInfo = auctionInfo;
    }
}
