package emiratsauction.com.ea_androidtask.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mahmoud on 07/07/2018.
 */

public class CarsOnline implements Serializable {
    @SerializedName("alertEn")
    String alertEn;
    @SerializedName("alertAr")
    String  alertAr;
    @SerializedName("Error")
    CarsOnlineError Error;
    @SerializedName("RefreshInterval")
    int RefreshInterval;
    @SerializedName("Ticks")
    String  Ticks;
    @SerializedName("count")
    int count;
    @SerializedName("endDate")
    int endDate;
    @SerializedName("sortOption")
    String sortOption;
    @SerializedName("sortDirection")
    String sortDirection;
    @SerializedName("Cars")
    ArrayList<Car> Cars;

    public String alertEn() {
        return alertEn;
    }

    public void setAlertEn(String alertEn) {
        this.alertEn = alertEn;
    }

    public String getAlertAr() {
        return alertAr;
    }

    public void setAlertAr(String alertAr) {
        this.alertAr = alertAr;
    }

    public CarsOnlineError getError() {
        return Error;
    }

    public void setError(CarsOnlineError error) {
        Error = error;
    }

    public int getRefreshInterval() {
        return RefreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        RefreshInterval = refreshInterval;
    }

    public String getTicks() {
        return Ticks;
    }

    public void setTicks(String ticks) {
        Ticks = ticks;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getSortOption() {
        return sortOption;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public ArrayList<Car> getCars() {
        return Cars;
    }

    public void setCars(ArrayList<Car> cars) {
        Cars = cars;
    }
}
