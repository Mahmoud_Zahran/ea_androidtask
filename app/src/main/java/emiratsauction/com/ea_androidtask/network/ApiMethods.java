package emiratsauction.com.ea_androidtask.network;

import android.content.Context;
import android.util.Log;

import emiratsauction.com.ea_androidtask.model.CarsOnline;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Mahmoud on 07/07/2018.
 */
public class ApiMethods {
    static public String sessionKey;
    static public CarsOnline data;

    public static CarsOnline CarsOnlineDataLoad(final Context context, final LoginCallback loginCallback,
                                 final boolean showDialog/*, String username, String password*/) {

//        final ProgressDialog progressDialog;
//        if (showDialog) {
//            progressDialog = CustomProgressDialog.create(context);
//            progressDialog.show();
//        } else {
//            progressDialog = null;
//        }

        Retrofit retrofit = RetrofitSingleton.getInstance();
        ApiInterfaces.CarListApi service = retrofit.create(ApiInterfaces.CarListApi.class);

        if (/*NetworkingUtils.isNetworkConnected()*/true) {
            Call<CarsOnline> call = service.getApiData();
            call.enqueue(new Callback<CarsOnline>() {
                @Override
                public void onResponse(Call<CarsOnline> call, Response<CarsOnline> response) {

                   data= response.body();
                    //Headers headers = response.headers().names();
                 //   String[] cookie = response.headers().toMultimap().get("set-cookie").get(0).split("\\;");

//                    if (showDialog) {
//                        progressDialog.dismiss();
//                    }
                }

                @Override
                public void onFailure(Call<CarsOnline> call, Throwable t) {
                    Log.d("onFailure", t.toString());
                    parseNetworkError(t, context);
                    loginCallback.loginData(false);
//                    if (showDialog) {
//                        progressDialog.dismiss();
//                    }
                }
            });
        } else {
//            if (showDialog) {
//                progressDialog.dismiss();
//            }
            //Toast.makeText(context, context.getString(R.string.noInternet), Toast.LENGTH_SHORT).show();
        }
        return data;

    }

//    public static void SecondUser(final Context context, final LoginCallback loginCallback,
//                                  final boolean showDialog) {
//
//        final ProgressDialog progressDialog;
//        if (showDialog) {
//            progressDialog = CustomProgressDialog.create(context);
//            progressDialog.show();
//        } else {
//            progressDialog = null;
//        }
//
//              Retrofit retrofit = RetrofitSingleton.getInstance();
//        ApiInterfaces.DataApi service = retrofit.create(ApiInterfaces.DataApi.class);
//
//        if (/*NetworkingUtils.isNetworkConnected()*/true) {
//            Call<String> call = service.getApiData(sessionKey/*"JSESSIONID=E0E97D01430AB08723CF79014D94A9A0; Path=/; HttpOnly"*/);
//            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<String> call, Response<String> response) {
//
//                    data=response.body();
//                    //Headers headers = response.headers().names();
//                    //headers.getDate()
//                    //String session = response.headers().namesAndValues[11];
//                    /*try {
//                        JSONObject jsonObject = new JSONObject(response.body());
//                        Gson gson = new Gson();
//                        if (jsonObject.getInt(ApiUrls.STATE_KEY) == ApiUrls.SUCCESS_RESPONSE_CODE) {
//                            LoginModel loginModel = gson.fromJson(jsonObject.getJSONObject(ApiUrls.DATA_KEY).toString(), LoginModel.class);
//                            loginCallback.loginData(true, loginModel);
//                        } else {
//
//                            loginCallback.loginData(false, null);
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }*/
//                    Toast.makeText(context, data, Toast.LENGTH_LONG).show();
//                    if (showDialog) {
//                        progressDialog.dismiss();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<String> call, Throwable t) {
//                    Log.d("onFailure", t.toString());
//                    parseNetworkError(t, context);
//                    loginCallback.loginData(false);
//                    if (showDialog) {
//                        progressDialog.dismiss();
//                    }
//                }
//            });
//        } else {
//            if (showDialog) {
//                progressDialog.dismiss();
//            }
//            //Toast.makeText(context, context.getString(R.string.noInternet), Toast.LENGTH_SHORT).show();
//        }
//    }


    public static void parseNetworkError(Throwable t, Context context) {

        /*if (t.getCause() instanceof IllegalStateException) {
            Toast.makeText(context, context.getString(R.string.formatError), Toast.LENGTH_SHORT).show();
            return;
        } else if (t.getCause() instanceof UnknownHostException) {
            Toast.makeText(context, context.getString(R.string.canNotReach), Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(context, context.getString(R.string.errorInternet), Toast.LENGTH_SHORT).show();*/

    }



    public interface LoginCallback {
        void loginData(boolean state/*, LoginModel loginResponse*/);
    }

}
