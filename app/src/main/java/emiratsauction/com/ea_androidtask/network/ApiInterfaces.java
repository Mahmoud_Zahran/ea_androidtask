package emiratsauction.com.ea_androidtask.network;


import emiratsauction.com.ea_androidtask.model.CarsOnline;
import retrofit2.Call;
import retrofit2.http.POST;


/**
 * Created by Mahmoud on 07/07/2018.
 */

public class ApiInterfaces {

    public interface CarListApi {

        @POST(ApiUrls.API_URL_CarList)
        Call<CarsOnline> getApiData();
    }
//    public interface LoadImage {
//
//        @POST(ApiUrls.API_URL_CarList)
//        Call<String> getApiData(@Query(ApiUrls.USERNAME_KEY) String data, @Query(ApiUrls.PASSWORD_KEY) String password);
//    }
//    public interface DataApi {
//       // @Headers("Cookie: JSESSIONID=C75533EBB11B1F6806F8390A5E85B3B3; Path=/; HttpOnly")
//        @POST(ApiUrls.API_URL2)
//        Call<String> getApiData(@Header("Cookie") String session);
//    }

}
